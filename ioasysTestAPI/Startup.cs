using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ioasysTestAPI.Models;
using Microsoft.AspNetCore.Identity;
using System.Text;
using System;
using Microsoft.AspNetCore.Authorization;


namespace ioasysTestAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EnterpriseContext>(opt => opt.UseInMemoryDatabase("Enterprise"));
            services.AddControllers();

            services.AddApiVersioning(o => 
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });


        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //var context = app.ApplicationServices.GetService<EnterpriseContext>();
            //AddTestData(context);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void AddTestData(EnterpriseContext context)
        {
            var testType = new EnterpriseType
            {
                id = 1,
                description = "retailer"
            };

            context.Enterprise_type.Add(testType);

            var testEnterprise = new Enterprise
            {
                Id = 1,
                Email_enterprise = "business@email.com",
                Facebook = "null",
                Twitter = "null",
                Linkedin = "null",
                Phone = "5555555",
                Own_enterprise = true,
                Enterprise_name = "ioasys",
                Photo = "null",
                Description = "The best software house in town!",
                City = "Lavras",
                Country = "Brazil",
                Enterprise_value = 11.0M,
                Share_price = 1.0M,
                Enterprise_type_Id = 1
            };

            context.Enterprise.Add(testEnterprise);

            context.SaveChanges();
        }
    }
}
