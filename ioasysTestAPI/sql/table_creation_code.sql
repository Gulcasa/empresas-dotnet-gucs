-- Autor: Guilherme Santana
-- Data: 27/09/2019

IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'DBioasys')
    CREATE database DBioasys
GO

USE [DBioasys]  
GO  

/****** Remove tabelas anteriores para evitar duplicatas e outros problemas ******/  
IF (EXISTS (SELECT * 
	FROM INFORMATION_SCHEMA.TABLES 
	WHERE TABLE_SCHEMA = 'dbo' 
	AND  TABLE_NAME = 'Login'))
BEGIN
	DROP TABLE [dbo].[Login]  
END
GO  

IF (EXISTS (SELECT * 
	FROM INFORMATION_SCHEMA.TABLES 
	WHERE TABLE_SCHEMA = 'dbo' 
	AND  TABLE_NAME = 'Enterprise'))
BEGIN
	DROP TABLE [dbo].[Enterprise]  
END
GO  

IF (EXISTS (SELECT * 
	FROM INFORMATION_SCHEMA.TABLES 
	WHERE TABLE_SCHEMA = 'dbo' 
	AND  TABLE_NAME = 'Enterprise_type'))
BEGIN
	DROP TABLE [dbo].[Enterprise_type]  
END
GO  

/****** Cria a tabela de Login, para armazenar os pares email/password permissíveis para login no sistema ******/  
SET ANSI_NULLS ON  
GO  
SET QUOTED_IDENTIFIER ON  
GO  
SET ANSI_PADDING ON  
GO  

CREATE TABLE [dbo].[Login](  
    [id] [int] IDENTITY(1,1) NOT NULL,  
    [email] [varchar](100) NOT NULL,  
    [password] [varchar](50) NOT NULL,  
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED   
(  
    [id] ASC  
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]  
) ON [PRIMARY]  
  
GO 

/****** Cria a tabela Enterprise_type, para armazenar os tipos de empresa disponíveis ******/  
SET ANSI_NULLS ON  
GO  
SET QUOTED_IDENTIFIER ON  
GO  
SET ANSI_PADDING ON  
GO  

CREATE TABLE [dbo].[Enterprise_type](  
    [id] [int] IDENTITY(1,1) NOT NULL,  
    [description] [varchar](100) NOT NULL,  
 CONSTRAINT [PK_Enterprise_type] PRIMARY KEY CLUSTERED   
(  
    [id] ASC  
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]  
) ON [PRIMARY]  
  
GO 

/****** Cria a tabela Enterprise, para armazenar os dados das empresas ******/  
SET ANSI_NULLS ON  
GO  
SET QUOTED_IDENTIFIER ON  
GO  
SET ANSI_PADDING ON  
GO  

CREATE TABLE [dbo].[Enterprise](  
    [Id] [int] IDENTITY(1,1) NOT NULL,  
    [Email_enterprise] [varchar](100) NOT NULL,  
    [Facebook] [varchar](100) NOT NULL,  
    [Twitter] [varchar](100) NOT NULL,  
    [Linkedin] [varchar](100) NOT NULL,  
    [Phone] [varchar](100) NOT NULL,  
    [Own_enterprise] [bit] NOT NULL,  
    [Enterprise_name] [varchar](100) NOT NULL,  
    [Photo] [varchar](100) NOT NULL,  
    [Description] [varchar](100) NOT NULL,  
    [City] [varchar](100) NOT NULL,  
    [Country] [varchar](100) NOT NULL,  
    [Enterprise_value] [decimal](10,3) NOT NULL,  
    [Share_price] [decimal](10,3) NOT NULL, 
    [Enterprise_type_Id] [int] NOT NULL, 
    FOREIGN KEY ([Enterprise_type_Id]) REFERENCES [dbo].[Enterprise_type](id), 
 CONSTRAINT [PK_Enterprise] PRIMARY KEY CLUSTERED
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
  
GO 