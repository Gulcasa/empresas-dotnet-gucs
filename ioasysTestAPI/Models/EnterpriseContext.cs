﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ioasysTestAPI.Models
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions<EnterpriseContext> options) : base(options)
        {
        }

        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> Enterprise_type { get; set; }
    }
}
