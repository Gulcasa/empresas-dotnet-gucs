﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasysTestAPI.Models
{
    // model class for the types of businesses
    public class EnterpriseType
    {
        public int id { get; set; }
        public string description { get; set; }
    }
}
